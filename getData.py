import serial
import pandas as pd
import time
import matplotlib.pyplot as plt
import matplotlib.animation as anm

def getNSave(starttime, conn, df, bit2mv):
    cc=str(conn.readline().decode())
    with open('./data{}.csv'.format(starttime), 'a+') as file:
        print(cc, file=file)
        print(cc[0:-1])
    ccsplit = cc.split()
    df.loc[len(df)]=[float(ccsplit[0]), float(ccsplit[1])*bit2mv, float(ccsplit[2])*bit2mv, float(ccsplit[3])*bit2mv, float(ccsplit[1])*bit2mv*(1/(4.28*2.2)), float(ccsplit[2])*bit2mv*(1/(4.28*2.2)), float(ccsplit[3])*bit2mv*(1/(4.28*2.2))]



conn = serial.Serial("/dev/ttyUSB0", 9600) #####<--------------- ustawiÄ‡ odpowiedni port
starttime = time.strftime("%y%m%d-%H%M%S")
with open('./data{}.csv'.format(starttime), 'a+') as file:
    print('t[s]\tU1[mV]\tU2[mV]\tU3[mV]',file=file)
df = pd.DataFrame(columns=['time', 'reading1 [mV]', 'reading2 [mV]', 'reading3 [mV]', 'eps1', 'eps2', 'eps3'])
bit2mv = 20/(2**23) ########<- -----------------------------------zaleĹĽne od wzmocnienia:
k = 2.12
U = 4.3
print('zeroing...(5s)')
for i in range(50):                                               # 128 - 20/2**23;
    getNSave(starttime, conn, df, bit2mv)                         #64 - 40/2**23;
statr1 = df['reading1 [mV]'].tail(45).mean()                      #32 - 80/2**23;
statr2 = df['reading2 [mV]'].tail(45).mean()
statr3 = df['reading3 [mV]'].tail(45).mean()
df = df[0:0] ############3<-------------------------------------usun dane z kalibracji
while 1:
    for i in range(10): #############<--------------------- 10 czyli 1 plot co 10 odczytĂłwarning
        getNSave(starttime, conn, df, bit2mv)              #jak bedzie za male to sie czas rozjedzie
    #df['eps1'] = df['reading1 [mV]']*4/(U*k)
    #df['eps2'] = df['reading2 [mV]']*4/(U*k)
    #df['eps3'] = df['reading3 [mV]']*4/(U*k)
    plt.clf()
    plt.subplot(2, 1, 1)
    plt.plot(df['time'].tail(250), df['reading1 [mV]'].tail(250)-statr1)
    plt.plot(df['time'].tail(250), df['reading2 [mV]'].tail(250)-statr2)
    plt.plot(df['time'].tail(250), df['reading3 [mV]'].tail(250)-statr3)
    plt.title('Przebieg napięć')
    plt.xlabel('czas [s]')
    plt.ylabel('wskazanie [mV]')
    plt.legend(['HX1', 'HX2', 'HX3'])
    plt.subplot(2, 1, 2)
    plt.plot(df['time'].tail(250), df['eps1'].tail(250))
    plt.plot(df['time'].tail(250), df['eps2'].tail(250))
    plt.plot(df['time'].tail(250), df['eps3'].tail(250))
    plt.xlabel('czas [s]')
    plt.ylabel('odkształcenie [-]')
    plt.title('Przebieg odkształceń')
    plt.show(block=False)
    plt.show(block=False)
    plt.pause(0.01)