#include "HX711.h"

// HX711 circuit wiring
const int DATA_PIN1 = 2;
const int CLOCK_PIN1 = 3;
const int DATA_PIN2 =4 ;
const int CLOCK_PIN2 = 5;
const int DATA_PIN3 = 8;
const int CLOCK_PIN3 = 9;


long reading1 = 0;
long reading2 = 0;
long reading3 = 0;

HX711 hx1;
HX711 hx2;
HX711 hx3;

void setup() {
  Serial.begin(9600);
  hx1.begin(DATA_PIN1, CLOCK_PIN1);
  hx2.begin(DATA_PIN2, CLOCK_PIN2);
  hx3.begin(DATA_PIN3, CLOCK_PIN3);
}

void loop() {
  if (hx1.is_ready()) {
    long reading = hx1.read();
    reading1 = reading;
    }
   else {
    reading1 = 0;
  }
  if (hx2.is_ready()) {
   long reading = hx2.read();
    reading2 = reading;
    }
   else {
    reading2 = 0;
  }
  if (hx3.is_ready()) {
    long reading = hx3.read();
    reading3 = reading;
    }
   else {
    reading3 = 0;
  }  
  
  Serial.print(millis()/1000.0);
  Serial.print("\t");
  Serial.print(reading1);
  Serial.print("\t");
  Serial.print(reading2);
  Serial.print("\t");
  Serial.print(reading3);
  Serial.print("\t");
  Serial.print("\r");
  Serial.print("\n");

  delay(100);
  
}