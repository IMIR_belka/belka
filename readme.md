# Program do arduino obsługujący trzy moduły hx711
### Wymagane biblioteki

https://github.com/bogde/HX711

### Schemat podłączenia
![](podlaczenie_3x.svg)


| ID modułu || pin SCK | pin DT |
| --- || --- | --- |
| 1 || 2 | 3|
| 2 || 4 | 5 |
| 3 || 8 | 9 |
